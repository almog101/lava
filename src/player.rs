use macroquad::{prelude::{Vec2, BLACK, GREEN}, window::{screen_width, screen_height}, shapes::draw_circle};

pub const PLAYER_RADIUS: f32 = 20.0;
pub const PLAYER_GRAVITY: f32 = 9.8;

pub enum PlayerState {
    Idle,
    Moving,
}

pub struct Player {
    pub position: Vec2,
    pub velocity: Vec2,
    pub state: PlayerState,
    pub can_move: bool,
}

impl Player {
    pub fn new() -> Player {
        return Player {
            position: Vec2 { x: screen_width()/2.0, y: screen_height() - PLAYER_RADIUS*2.0 },
            velocity: Vec2::ZERO,
            state: PlayerState::Idle,
            can_move: true,
        };
    }

    pub fn draw(&self) {
        if self.can_move {
            draw_circle(self.position.x, self.position.y, PLAYER_RADIUS, GREEN);
        } else {
            draw_circle(self.position.x, self.position.y, PLAYER_RADIUS, BLACK);
        }
    }

    pub fn update(&mut self, delta_time: f32) {
        match self.state {
            PlayerState::Moving => {
                self.velocity.y += PLAYER_GRAVITY * delta_time;
                self.velocity.y = self.velocity.y.min(10.0);
                self.position += self.velocity;

                if self.position.x-PLAYER_RADIUS < 0.0 {
                    self.position.x = PLAYER_RADIUS;
                    self.velocity.x = -self.velocity.x/2.0;
                } else if self.position.x+PLAYER_RADIUS > screen_width() {
                    self.position.x = screen_width() - PLAYER_RADIUS;
                    self.velocity.x = -self.velocity.x/2.0;
                }
            },
            PlayerState::Idle => (),
        }
    }
}
