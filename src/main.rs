mod player;
mod wall;

use macroquad::{prelude::*, rand::{self, srand}, time};
use player::{Player, PlayerState};
use wall::Wall;

macro_rules! mouse_pos {
    () => {
        {
            let tmp = mouse_position();
            Vec2 {x: tmp.0, y: tmp.1}
        }
    };
}

fn gen_wall(walls: &Vec<Wall>) -> Wall {
    let len = screen_width()* rand::gen_range(0.2, 0.5);
    let x = rand::gen_range(len, screen_width()-len);
    
    let heighest_wall = walls.iter().min_by(|w1, w2| w1.position.y.total_cmp(&w2.position.y) );

    match heighest_wall {
        Some(w) => {
            let edges = w.edges();
            let min_y = edges.0.y.min(edges.1.y);
            return Wall::new(x, min_y - len, len, 12.0, rand::gen_range(0.0, 360.0));
        }
        None => return Wall::new(x, screen_height()/1.5, len, 12.0, rand::gen_range(0.0, 360.0))
    }
}

async fn game_loop() {
    let mut player = Player::new();
    let mut delta_time;

    let mut mouse_start_pos = vec2(-1.0, -1.0);
    let mut mouse_end_pos;

    let mut cam = Camera2D {
        zoom: vec2(2.0/screen_width(), -2.0 / screen_height()),
        target: vec2(screen_width(), screen_height()) /2.0,
        ..Default::default()
    };
    let cam_screen_offset = cam.target.y;
    let mut score = "0".to_string();

    let mut walls: Vec<Wall> = vec![];
    walls.push(gen_wall(&walls));
    walls.push(gen_wall(&walls));
    walls.push(gen_wall(&walls));
    loop {
        // Update
        delta_time = get_frame_time();
        player.update(delta_time);

        if cam.target.y + screen_height() < player.position.y {
            return;
        }

        // if player reaches the top of the screen,
        // move the camera y offset
        if cam.target.y > player.position.y + cam_screen_offset {
            score = ( (score.parse::<f32>().unwrap() + (cam.target.y - (player.position.y+ cam_screen_offset))) as u32 ).to_string();
            cam.target.y = player.position.y + cam_screen_offset;
        }

        // calculate the move direction vector
        if is_mouse_button_pressed(MouseButton::Left) && player.can_move {
            mouse_start_pos = mouse_pos!();
        }
        mouse_end_pos = mouse_pos!();

        if is_mouse_button_released(MouseButton::Left) && player.can_move {
            player.velocity += (mouse_end_pos - mouse_start_pos).normalize() * ((player.velocity.y.abs() * 3.0).max(15.0));
            mouse_start_pos.x = -1.0;
            player.state = PlayerState::Moving;
            player.can_move = false;
        }

        for wall in &walls {
            if wall.intersects_with_player(&player) {
                player.position += wall.collision_normal(&player);
                player.velocity += wall.collision_normal(&player);
                player.can_move = true;
            }
        }

        // Draw
        clear_background(LIGHTGRAY);
        set_camera(&cam);

        player.draw();
        
        // draw move direction
        if mouse_start_pos.x != -1.0 {
            let pos = player.position + (mouse_end_pos - mouse_start_pos).normalize() * 100.0;
            draw_circle(pos.x, pos.y, 10.0, RED);
        }
        for wall in &walls {
            wall.draw();
        }

        set_default_camera();
        draw_text(&score, 20.0, 30.0, 45.0, BLACK);

        next_frame().await
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    simulate_mouse_with_touch(true);
    srand((time::get_time()* (0x1234567 as f64)) as u64);

    loop {
        game_loop().await;
    }
}

fn window_conf() -> Conf {
    Conf {
        window_title: "lava!".to_string(),
        fullscreen: false,
        window_resizable: false,
        window_width: 540,
        window_height: 960,
        ..Default::default()
    }
}
