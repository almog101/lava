use macroquad::{prelude::{BLUE, Vec2, vec2, WHITE, RED}, texture::{Texture2D, Image, draw_texture_ex, DrawTextureParams}, shapes::draw_circle};

use crate::player::{Player, PLAYER_RADIUS};

pub struct Wall {
    pub position: Vec2,
    pub length: f32,
    pub radius: f32,
    pub angle: f32,
    x_offset: f32,
    texture: Texture2D,
}

impl Wall {
    pub fn new(x: f32, y:f32, length: f32, radius: f32, angle: f32) -> Wall {
        Wall { 
            position: vec2(x, y),
            length: length,
            radius: radius,
            angle: angle,
            x_offset: length/2.0,
            texture: Texture2D::from_image(
                &Image::gen_image_color(length as u16 , (radius*2.0) as u16, BLUE)
            ) 
        }
    }

    pub fn edges(&self) -> (Vec2, Vec2) {

        return (
        vec2( self.position.x + self.angle.cos()*self.x_offset, self.position.y + self.angle.sin()*self.x_offset ), 
        vec2( self.position.x - self.angle.cos()*self.x_offset, self.position.y - self.angle.sin()*self.x_offset )
        );
    }
    pub fn draw(&self) {
        draw_texture_ex(self.texture, self.position.x - self.x_offset, self.position.y-self.radius, WHITE, DrawTextureParams {rotation:self.angle, ..Default::default()});

        let edges = self.edges();
        draw_circle(edges.0.x, edges.0.y, self.radius, RED);
        draw_circle(edges.1.x, edges.1.y, self.radius, RED);
    }

    pub fn player_closest_point(&self, player: &Player) -> Vec2 {
        let m = (self.angle).tan();
        let mut closest_point = Vec2::ZERO;

        if m == 0.0 {
            closest_point.x = player.position.x;
            closest_point.y = self.position.y;
        } else {
            closest_point.x = (player.position.y - self.position.y + m*self.position.x + player.position.x/m) / (m + 1.0/m);
            closest_point.y = m * (closest_point.x - self.position.x) + self.position.y;
        }
        
        let edges = self.edges();
        closest_point.x = closest_point.x.clamp(
                edges.0.x.min(edges.1.x),
                edges.0.x.max(edges.1.x)
        );

        closest_point.y = closest_point.y.clamp(
                edges.0.y.min(edges.1.y),
                edges.0.y.max(edges.1.y)
        );

        closest_point
    }

    pub fn intersects_with_player(&self, player: &Player) -> bool {
        let closest_point = self.player_closest_point(player);
        return (closest_point.x - player.position.x).powi(2) + (closest_point.y - player.position.y).powi(2) <= (PLAYER_RADIUS + self.radius).powi(2);
    }

    pub fn collision_normal(&self, player: &Player) -> Vec2 {
        let closest_point = self.player_closest_point(player);
        return (player.position - closest_point).normalize();
    }
}
